@extends('layouts.app')

@section('content')
<div class="container" style="margin-top: 50px; background-color: unset;">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <p class="text-danger text-center">Please log with the username "userYOUR-USER-NUMBER@googlemail.com". The password is "password".</p>

                    <form method="POST" action="{{ route('login') }}" autocomplete="off">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">

                                <p class="text-danger">Complete Experiment 1 first, then Experiment 2 after.</p>
                                <input name="experiment" value="1" type="hidden">

                                <button type="submit" class="btn btn-primary" name="experiment" value="1">
                                    {{ __('Log in for Experiment 1') }}
                                </button>

                                <button type="submit" class="btn btn-primary" name="experiment" value="2">
                                    {{ __('Log in for Experiment 2') }}
                                </button>
                            </div>
                        </div>
                    </form>

                    <hr>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                            
                                <a class="btn btn-link" href="{{ route('register') }}">
                                    {{ __('Not signed up yet? Register here.') }}
                                </a>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
