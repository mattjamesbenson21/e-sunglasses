<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>e-Sunglasses</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>

    <body style="margin-top: 50px; background-color: unset;">
        @include('layouts.app')

        <div class="container">
            @include('flash::message')
        </div>

        <div class="flex-center position-ref">
            <div class="content" id="topPicks">
                <div class="display-4">
                    {{ config('app.name') }}
                </div>
                
                <br>

                @if(App\User::where('id', Auth::id())->where('experiment', 'second')->count() == 0)
                    <div class="links">
                        <a href="{{ route('new-releases') }}">New Releases</a>
                        <a href="{{ route('mens') }}">Men's Sunglasses</a>
                        <a href="{{ route('womens') }}">Women's Sunglasses</a>
                        <a href="{{ route('children') }}">Children's Sunglasses</a>
                        <a href="{{ route('sale') }}">Sale</a>
                    </div>
                @endif
            </div>
        </div>

        <div class="container">
            <hr>
            @yield('content')
        </div>

        <br>
    </body>
</html>